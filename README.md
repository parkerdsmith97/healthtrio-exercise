# HealthTrio exercise

Using the language of your choice, please connect to the appropriate endpoint and print out, by state and in descending order, the percentage of eligible and critical access hospitals that have demonstrated Meaningful Use of CEHRT in the year 2014. 